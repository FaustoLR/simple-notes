import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule, Router } from '@angular/router';
import { MisNotasPage } from './page/mis-notas/mis-notas.page';
import { NotasArchivadasPage } from './page/notas-archivadas/notas-archivadas.page';
import { NotasEliminadasPage } from './page/notas-eliminadas/notas-eliminadas.page';

const routes: Routes = [
  {
    path:'',
    children: [
      {
        path:'mis-notas',
        component: MisNotasPage
      },
      {
        path:'archivado',
        component: NotasArchivadasPage
      },
      {
        path:'eliminado',
        component: NotasEliminadasPage
      },
      {
        path:'**',
        redirectTo: 'mis-notas'
      }
    ]
  }
]
  


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forChild( routes )
  ],
  exports: [
    RouterModule
  ]
})
export class ModulosRoutingModule { 
  constructor(private router: Router) {
    router.events.subscribe((val:any) => {
      if(window.location.href.includes("http:") && !window.location.href.includes("localhost")){
        location.href="https://plusbarrent.com.mx"+val.url;
      }
    });
  }

}

