import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NotasEliminadasPage } from './notas-eliminadas.page';

describe('NotasEliminadasPage', () => {
  let component: NotasEliminadasPage;
  let fixture: ComponentFixture<NotasEliminadasPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NotasEliminadasPage ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NotasEliminadasPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
