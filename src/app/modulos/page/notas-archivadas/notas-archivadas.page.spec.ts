import { ComponentFixture, TestBed } from '@angular/core/testing';

import {NotasArchivadasPage } from './notas-archivadas.page';

describe('NotasArchivadasComponent', () => {
  let component: NotasArchivadasPage;
  let fixture: ComponentFixture<NotasArchivadasPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NotasArchivadasPage ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NotasArchivadasPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
