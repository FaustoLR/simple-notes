import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ModulosRoutingModule } from './modulos-routing.module';
import { ReactiveFormsModule } from '@angular/forms';
import { MisNotasPage } from './page/mis-notas/mis-notas.page';
import { NotasArchivadasPage } from './page/notas-archivadas/notas-archivadas.page';
import { NotasEliminadasPage } from './page/notas-eliminadas/notas-eliminadas.page';
import { NotasComponent } from '../componentes/notas/notas.component';
import { FormsModule } from '@angular/forms';
import {NgbPaginationModule, NgbAlertModule, NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { ModalConfirmacion } from '../componentes/modal/modal.component';
import { ToastsComponent } from '../componentes/toast/toast.component';

@NgModule({
  declarations: [
    MisNotasPage,
    NotasArchivadasPage,
    NotasEliminadasPage,
    NotasComponent,
    ModalConfirmacion,
    ToastsComponent
  ],
  imports: [
    CommonModule,
    ModulosRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    NgbModule,
    NgbPaginationModule, 
    NgbAlertModule
  ],
  exports:[],
})
export class ModulosModule { }
