export interface Nota{
    titulo : string;
    contenido : string;
    fecha : Date;
}