import { FormControl, FormGroup, ValidationErrors } from "@angular/forms";

// Regex para validar si existen correos en una cadena
const regex = /([a-zA-Z0-9._-]+@[a-zA-Z0-9._-]+\.[a-zA-Z0-9._-]+)/gi;

export class ValidacionesPropias {
    /**
     * @method Valida si en un input se agrego un correo electrónico.
     * @param formulario : Elemento del FormGroup que contiene la información del input
     * @return null : Si no se encuentra un correo en el input
     * @return { existeEmail : true } : Si el input contiene correos
     */
    static ValidatorIsEmailInInput(formulario: FormGroup): ValidationErrors | null {
        if (formulario.value == null || formulario.value.match(regex) == null){
            return null;
        }
        return { existeEmail : true }
    }

    /**
     * @method Valida si una cadena tiene un correo electrónico.
     * @param cadena : Cadena a evaluar
     * @return arreglo con correos, si retorna null no hay correos en la cadena
     */
    static existeEmail(cadena : string) : string[] | null{
        return cadena.match(regex);
    }
  }

