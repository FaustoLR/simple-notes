import { Injectable } from '@angular/core';
import { Nota } from '../interfaces/Nota.interface';

@Injectable({
  providedIn: 'root'
})
export class NotaService {
  // Variable para almacenar las notas.
  private notas : Nota[] = [];
  // Variable para almacenar las notas archivadas.
  private notasArchivadas : Nota[] = [];
  // Variable para almacenar las notas eliminadas.
  private notasEliminadas : Nota[] = [];


  constructor() { 
    this.notas = this.getNotasLocales("notas");
    this.notasArchivadas = this.getNotasLocales("notasArchivadas");
    this.notasEliminadas = this.getNotasLocales("notasEliminadas");
  }

  /**
   * @method Método para obtener una colección de notas del localStorage
   * @param key : Valor clave a obtener del localStorage
   * @returns : Colección de notas
   */
  private getNotasLocales(key : string) : Nota[]{
    let notas:Nota[] = [];
    let str = localStorage.getItem(key);
    if(str != null){
      notas = JSON.parse(str);
    }
    return notas;
  }

  /**
   * @method Método que agrega una nueva nota a una colección y la guarda en el localStorage
   * @param nota : Nota que será insertada
   */
  addNota(nota : Nota) : void {
    this.notas.push(nota);
    localStorage.setItem("notas", JSON.stringify(this.notas));
  }

  /**
   * @method Método que archiva una nota
   * @param key : Key de la colección del localStorage
   * @param nota : Nota que será archivada
   * @param index : Posición en el arreglo de notas
   */
  archivaNota(key : string, nota : Nota, index : number):void{
    this.notasArchivadas.push(nota);
    let valorStr = "[]";
    switch(key){
      case "notas":
        this.notas.splice(index,1);
        valorStr = JSON.stringify(this.notas);
        break;
      case "notasEliminadas":
        this.notasEliminadas.splice(index,1);
        valorStr = JSON.stringify(this.notasEliminadas);
        break;
    }
    localStorage.setItem(key, valorStr);
    localStorage.setItem("notasArchivadas", JSON.stringify(this.notasArchivadas));
  }

  /**
   * @method Método que elimina temporalmente una nota y la manda a la colección de notas eliminadas 
   * @param key : Key de la colección del localStorage
   * @param nota : Nota que será mandada a la colección de notas eliminadas
   * @param index : Posición en el arreglo de notas
   */
  eliminaNotaTemp(key : string, nota : Nota, index : number):void{
    let valorStr = "[]";
    switch(key){
      case "notas":
        this.notas.splice(index,1);
        valorStr = JSON.stringify(this.notas);
        break;
      case "notasArchivadas":
        this.notasArchivadas.splice(index,1);
        valorStr = JSON.stringify(this.notasArchivadas);
        break;
    }
    this.notasEliminadas.push(nota);
    localStorage.setItem(key,valorStr);
    localStorage.setItem("notasEliminadas",JSON.stringify(this.notasEliminadas));
  }
  
  /**
   * @method Método para editar una nota
   * @param key : Key de la colección del localStorage
   */
  editaNota(key : string):void{
    let valorStr = "[]";
    switch(key){
      case "notas":
        valorStr = JSON.stringify(this.notas);
        break;
      case "notasArchivadas":
          valorStr = JSON.stringify(this.notasArchivadas);
          break;
    }
    localStorage.setItem(key,valorStr);
  }
  
  /**
   * @method Método que elimina permanentemente una nota
   * @param index : Posición en el arreglo de notas
   */
  eliminaNotaPermanente(index : number):void{
    this.notasEliminadas.splice(index,1);
    localStorage.setItem("notasEliminadas",JSON.stringify(this.notasEliminadas));
  }

  /**
   * @method Método que retorna la colección de notas
   * @returns Colección de notas
   */
  getNotas() : Nota[]{
    return this.notas;
  }

  /**
   * @method Método que retorna la colección de notas archivadas
   * @returns Colección de notas
   */
  getNotasArchivadas() : Nota[]{
    return this.notasArchivadas;
  }

  /**
   * @method Método que retorna la colección de notas eliminadas
   * @returns Colección de notas
   */
  getNotasEliminadas() : Nota[]{
    return this.notasEliminadas;
  }

}
