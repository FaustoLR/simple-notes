import {Component, ElementRef, ViewChild} from '@angular/core';

import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import { NotaService } from 'src/app/servicios/nota.service';
import { ToastService } from 'src/app/servicios/toast.service';

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html'
})
export class ModalConfirmacion {
  //Enlace con el modal del html
  @ViewChild('contentModal') modalHtml: ElementRef | any;
  //Posición de la nota en la colección de notas eliminadas
  index:number | undefined;

  constructor(private modalService: NgbModal, private _notaService : NotaService, public toastService : ToastService) {}
  
  /**
   * @method Método para abrir el modal de confirmación.
   * @param index : Posición de la nota en la colección de notas eliminadas
   */
  open(index : number) {
    this.index = index;
    this.modalService.open(this.modalHtml);
  }
  
  /**
   * @method Método para eliminar permanentemente la nota
   */
  eliminar(){
    if(this.index != undefined){
      this._notaService.eliminaNotaPermanente(this.index);
      this.toastService.show('Nota eliminada.', { classname: 'bg-success text-light', delay: 2000 });
    }
  }
}
