import { Component, ElementRef, Input, OnInit, ViewChild } from '@angular/core';
import { Validators, FormGroup, FormControl, ValidationErrors, ValidatorFn } from '@angular/forms';
import { Nota } from 'src/app/interfaces/Nota.interface';
import { NotaService } from 'src/app/servicios/nota.service';
import { ToastService } from 'src/app/servicios/toast.service';
import { ValidacionesPropias } from 'src/app/Validators/nota.validator';
import { ModalConfirmacion } from '../modal/modal.component';
declare var $:any;


@Component({
  selector: 'app-notas',
  templateUrl: './notas.component.html',
  styleUrls: ['./notas.component.scss']
})
export class NotasComponent implements OnInit {
  // Enlace con el modal de confirmación
  @ViewChild(ModalConfirmacion) modal: ModalConfirmacion | any;
  // Objeto FormGroup que controla el formulario para agregar nuevas notas.
  notaForm:FormGroup;
  // Variable que contiene la sección de la pagina donde se encuentra el usuario, 
  // de esta manera el componente tendrá un comportamiento personalizado para cada ruta.
  @Input() seccion:string = "notas";
  // Variable que contiene las notas que se mostraran
  notas : Nota[] = [];
  // Variable de respaldo cuando se edita o elimina una nota.
  editarEliminarNota : string = "{}";

  constructor(private _notaService : NotaService, public toastService : ToastService){ 
    // Agregamos las validaciones del formulario.
    this.notaForm = new FormGroup({
      titulo: new FormControl('', Validators.compose([
        Validators.required,
        ValidacionesPropias.ValidatorIsEmailInInput
      ])),
      contenido: new FormControl('', Validators.compose([
        Validators.required
      ]))
    });

  }

  ngOnInit(){
    switch(this.seccion){
      case "notas":
        this.notas = this._notaService.getNotas();
        break;
      case "notasArchivadas":
        this.notas = this._notaService.getNotasArchivadas();
        break;
      case "notasEliminadas":
        this.notas = this._notaService.getNotasEliminadas();
        break;
    }
  }

  /**
   * @method Método que crea una nueva nota
   */
  addNota():void{
    // Validamos que el formulario sea valido
    if(this.notaForm.valid){
      // Creamos la nota
      let nota : Nota = {
        titulo : this.notaForm.controls["titulo"].value,
        contenido : this.notaForm.controls["contenido"].value,
        fecha : new Date()
      }
      this._notaService.addNota(nota);
      this.notaForm.reset();
      this.notaForm.markAsUntouched();
      this.toastService.show('Nota creada.', { classname: 'bg-success text-light', delay: 2000 });
    }
  }

  /**
   * @method Método para eliminar una nota, si la sección esta en mis notas o archivado se envían 
   * a la colección de notas eliminadas, si está en la sección notas eliminadas se elimina de forma permanente
   * @param index : Índice de la nota
   * @param nota : Objeto de la nota
   */
  eliminaNota(index : number, nota : Nota):void {
    if(this.seccion == 'notasEliminadas'){
      // Se elimina permanentemente
      this.modal.open(this.modal);
    }else{
      // Se manda a la colección de notas eliminadas
      this._notaService.eliminaNotaTemp(this.seccion, nota, index);
      this.toastService.show('Nota eliminada.', { classname: 'bg-success text-light', delay: 2000 });
    }
  }

  /**
   * @method Método para editar una nota y validar que el titulo no tenga correos
   * @param nota 
   */
  editaNota(nota : Nota):void {
    let notaRespaldo : Nota = JSON.parse(this.editarEliminarNota);
    // Se valida si hay cambios en la nota para hacer la actualización.
    if(notaRespaldo.titulo != nota.titulo || notaRespaldo.contenido != nota.contenido){
      // Se valida que el titulo no tenga correos.
      let emails = ValidacionesPropias.existeEmail(nota.titulo);
      if(emails != null){// existen emails en la cadena
        // Se elimina del titulo
        emails.forEach(val => {
          nota.titulo = nota.titulo.replace(val,"");
        });
        nota.titulo = nota.titulo.trim();
      }
      // se guarda el cambio
      this._notaService.editaNota(this.seccion);
      this.toastService.show('Nota editada.', { classname: 'bg-success text-light', delay: 2000 });
    }
  }

  /**
   * @method Método que archiva una nota
   * @param index : Posición en el arreglo de notas
   * @param nota : Nota que será archivada
   */
  archivaNota(index : number, nota : Nota):void{
    this._notaService.archivaNota(this.seccion, nota, index);
    this.toastService.show('Nota archivada.', { classname: 'bg-success text-light', delay: 2000 });
  }

  /**
   * @method Método para respaldar una nota para validarla cuando es editada. Se guarda como string ya que se enlazan las variables cuando son objetos.
   * @param nota : Nota a respaldar
   */
  respaldaNota(nota : Nota):void{
    // Se respalda de esta manera para evitar el enlace con el ngmodel
    this.editarEliminarNota = JSON.stringify(nota);
  }
}
